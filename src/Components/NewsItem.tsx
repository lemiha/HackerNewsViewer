import { dateFromTimestamp } from '@/Shared/Utils/uils'
import { INewsItem } from '@/Store/News/types'
import { Badge, Box, Flex, HStack, Pressable, Spacer, Text } from 'native-base'
import React from 'react'
import { Linking, View } from 'react-native'

interface NewsItemProps {
  story: INewsItem
}

const NewsItem = ({ story }: NewsItemProps) => {
  return (
    <View>
      <Box alignItems="center" paddingBottom="1" paddingTop="2">
        <Pressable
          onPress={() => {
            if (story.url != null) {
              Linking.openURL(story.url)
            }
          }}
          width="93%"
        >
          <Box
            borderWidth="1"
            shadow="3"
            borderColor="coolGray.300"
            bg="coolGray.100"
            p="5"
            rounded="8"
          >
            <HStack alignItems="center">
              <Badge
                colorScheme="orange"
                _text={{
                  color: 'white',
                  fontWeight: 'extrabold',
                  fontSize: 'sm',
                }}
                variant="solid"
                width={60}
                rounded="4"
              >
                {story.storyScore}
              </Badge>
              <Spacer />
              <Text fontSize={10} color="coolGray.800">
                {dateFromTimestamp(story.timestamp).toISOString()}
              </Text>
            </HStack>
            <Text color="coolGray.800" mt="3" fontWeight="medium" fontSize="xl">
              {story.title}
            </Text>
            <Text mt="2" fontSize="sm" color="coolGray.500">
              <Text fontStyle="italic">Author: </Text>
              <Text>
                {story.author}
                {'\n'}
              </Text>
              <Text fontStyle="italic">Author's karma: </Text>
              <Text>{story.authorKarmaScore ?? '-'}</Text>
            </Text>
            <Flex>
              <Text mt="2" fontSize={12} fontWeight="medium" color="#0000EE">
                {story.url ?? '-'}
              </Text>
            </Flex>
          </Box>
        </Pressable>
      </Box>
    </View>
  )
}

export default NewsItem
