import React from 'react'
import { View, Image } from 'react-native'

interface Props {
  height?: number | string
  width?: number | string
  mode?: 'contain' | 'cover' | 'stretch' | 'repeat' | 'center'
}

const Logo = ({ height, width, mode }: Props) => {
  return (
    <View style={{ height, width }}>
      <Image
        style={{ width: '100%' }}
        source={require('@/Assets/Images/hackernews.png')}
        resizeMode={mode}
      />
    </View>
  )
}

Logo.defaultProps = {
  height: 200,
  mode: 'contain',
  width: 200,
}

export default Logo
