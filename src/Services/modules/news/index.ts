import { NUMBER_OF_STORIES_TO_SHOW } from '@/Shared/Constants/constants'
import { getRandomSubarray, notEmpty } from '@/Shared/Utils/uils'
import { payloadToNewsItem } from '@/Services/modules/news/utils'
import { INewsItemPayload, IUserPayload } from './types'

const API_URL = 'https://hacker-news.firebaseio.com/v0/'

export const fetchNewsStories = () => {
  let payloadItems: Array<INewsItemPayload>

  try {
    return fetch(API_URL + 'topstories.json')
      .then(function (response) {
        return response.json()
      })
      .then(function (data) {
        const storyIds: Array<number> = data

        const randomNewsItemIds: Array<number> = getRandomSubarray(
          storyIds,
          NUMBER_OF_STORIES_TO_SHOW,
        )

        return Promise.all(
          randomNewsItemIds.map(async id => {
            try {
              const payloadItem: INewsItemPayload = await fetch(
                API_URL + `item/${id}.json`,
              ).then(data => data.json())

              return payloadItem
            } catch {
              return null
            }
          }),
        )
      })
      .then(function (response) {
        // Filter to get unique author id's in case of duplicates
        payloadItems = response.filter(notEmpty)

        const authorUsernames = [
          ...new Set(payloadItems.map(newsItem => newsItem.by)),
        ]

        // Create array of authorsIds and their corresponding karma score
        return Promise.all(
          authorUsernames.map(async username => {
            try {
              const user: IUserPayload = await fetch(
                API_URL + `user/${username}.json`,
              ).then(data => data.json())

              return { key: username, value: user.karma }
            } catch {
              return null
            }
          }),
        )
      })
      .then(function (authorKarmaScoresArray) {
        // Remove nulls and transform array into a Map between the authorIds and their karma
        const authorKarmaScoresMap = new Map(
          authorKarmaScoresArray
            .filter(notEmpty)
            .map((score): [string, number] => [score.key, score.value]),
        )

        return payloadToNewsItem(payloadItems, authorKarmaScoresMap)
      })
  } catch {
    return null
  }
}
