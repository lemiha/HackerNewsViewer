import { INewsItemPayload } from '@/Services/modules/news/types'
import { INewsItem } from '../../../Store/News/types'

export const payloadToNewsItem = (
  newsPayload: Array<INewsItemPayload>,
  userKarmaScore: Map<string, number>,
): INewsItem[] => {
  const newsItems = newsPayload.map(item => {
    const newsItem: INewsItem = {
      id: item.id,
      title: item.title,
      url: item.url,
      timestamp: item.time,
      storyScore: item.score,
      author: item.by,
      authorKarmaScore: userKarmaScore.get(item.by),
    }

    return newsItem
  })

  newsItems.sort((a, b) => {
    return b.storyScore - a.storyScore
  })

  return newsItems
}
