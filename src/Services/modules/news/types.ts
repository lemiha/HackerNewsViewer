export interface INewsItemPayload {
  by: string
  descendants: number
  id: number
  kids: Array<number>
  score: number
  time: number
  title: string
  type: string
  url: string
}

export interface IUserPayload {
  created: number
  id: string
  karma: number
  submitted: Array<number>
}
