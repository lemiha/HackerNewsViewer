// https://stackoverflow.com/a/19270021
export const getRandomSubarray = (arr: Array<any>, n: number): Array<any> => {
  var result = new Array(n),
    len = arr.length,
    taken = new Array(len)
  if (n > len)
    throw new RangeError(
      'getRandomSubarray: more elements taken than available',
    )
  while (n--) {
    var x = Math.floor(Math.random() * len)
    result[n] = arr[x in taken ? taken[x] : x]
    taken[x] = --len in taken ? taken[len] : len
  }
  return result
}

// https://stackoverflow.com/a/46700791
export const notEmpty = <TValue>(
  value: TValue | null | undefined,
): value is TValue => {
  if (value === null || value === undefined) return false
  return true
}

export const dateFromTimestamp = (timestamp: number): Date => {
  const dateFromTimestamp = new Date(timestamp * 1000)

  return dateFromTimestamp
}
