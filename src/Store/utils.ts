import {
  ActionReducerMapBuilder,
  AsyncThunk,
  isFulfilled,
  isPending,
} from '@reduxjs/toolkit'
import { LoadingStatus } from './types'

export const handleLoadingStates = (
  builder: ActionReducerMapBuilder<LoadingStatus>,
  trunk: AsyncThunk<any, void, {}>,
) => {
  builder.addMatcher(isPending(trunk), state => {
    state.loading = true
  })
  builder.addMatcher(isFulfilled(trunk), state => {
    state.loading = false
  })
}
