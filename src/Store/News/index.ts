import { fetchNewsStories } from '@/Services/modules/news'
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { handleLoadingStates } from '../utils'
import { NewsState } from './types'

const newsStoriesInitialState: NewsState = {
  stories: null,
  loading: false,
}

export const getNewsStories = createAsyncThunk(
  'newsStories/getNewsStories',
  async thunkAPI => {
    return fetchNewsStories()
  },
)

const newsStoriesSlice = createSlice({
  name: 'newsStories',
  initialState: newsStoriesInitialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getNewsStories.fulfilled, (state, { payload }) => {
      state.stories = payload
    })
    handleLoadingStates(builder, getNewsStories)
  },
})

export default newsStoriesSlice.reducer
