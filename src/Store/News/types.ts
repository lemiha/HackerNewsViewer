import { LoadingStatus } from '../types'

export interface INewsItem {
  id: number
  title: string
  url: string | null | undefined
  timestamp: number
  storyScore: number
  authorKarmaScore: number | null | undefined
  author: string
}

export interface NewsState extends LoadingStatus {
  stories: Array<INewsItem> | null | undefined
}
