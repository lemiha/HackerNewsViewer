import { Logo } from '@/Components'
import { navigate } from '@/Navigators/utils'
import { getNewsStories } from '@/Store/News'
import { Spinner } from 'native-base'
import React, { useEffect } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

const StartupContainer = () => {
  const dispatch = useDispatch()

  const init = async () => {
    await dispatch(getNewsStories())
    navigate('Main', undefined)
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <View
      style={[
        {
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        },
      ]}
    >
      <Logo />
      <Spinner size="lg" style={{ marginVertical: 40 }} color="#FF6600" />
    </View>
  )
}

export default StartupContainer
