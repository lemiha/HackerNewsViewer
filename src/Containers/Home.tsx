import React from 'react'
import { RefreshControl } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { NewsItem } from '@/Components'
import { notEmpty } from '@/Shared/Utils/uils'
import { ScrollView } from 'native-base'
import { getNewsStories } from '@/Store/News'
import { NewsState } from '@/Store/News/types'

const Home = () => {
  const dispatch = useDispatch()

  const { stories, loading } = useSelector(
    (state: { news: NewsState }) => state.news,
  )

  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={loading}
          onRefresh={() => {
            dispatch(getNewsStories())
          }}
        />
      }
    >
      {notEmpty(stories) &&
        stories.map(story => <NewsItem key={story.id} story={story} />)}
    </ScrollView>
  )
}

export default Home
