# HackerNewsViewer

## Introduction

This is a React Native (RN) app that displays the top ten random news articles from the social news website Hacker News. The UI displays the following information in each of the ten news stories:

- Title of the news article
- The URL of the article
- Timestamp of the article
- Score of the article
- Author ID
- Author's karma score

This information is collected from three different API endpoints. The news stories are sorted in descending order by their score.

## Solution

### Architecture & Structure

This implementation is based on the RN boilerplate template from [TheCodingMachine](https://thecodingmachine.github.io/react-native-boilerplate/). This boilerplate is one of the most popular templates in the RN community for starting a new RN project, as it has some handy prerequisites and is easily scalable. The code is structured to separate concerns of the user interface and the business logic. E.g. navigation, API services, presentation components, containers, and the Redux store are kept separate.

The following libraries are used:

- Redux and redux-toolkit for state management.
- React navigation for navigation between screens/containers
- NativeBase for UI elements

The following folders under the _/src_ represent:

- **Services**
  - Communication with external API
- **Store**
  - State management with redux and redux-toolkit`.
- **Navigation**
  - Navigational logic between screens/containers
- **Container**
  - Screens/containers
- **Components**
  - Presentational components used in containers
- **Common**
  - Globally used utility functions and constants
- **Assests**
  - Files, such as images

### Additional functions

The following additional features have been implemented in addition to those mentioned in the introduction:

- Scroll down from the top to refresh the view with ten new news stories
- Start screen with the Hacker News logo with loading indicator
  - The screen switches to display the news articles when they are loaded
- Press on an article (if it has a link) to open the link in a browser

## Installation

The app was developed exclusively with an Android phone because no Mac or iPhone was available.

### APK

The APK is located in the _android/app/release_ folder with the name _app-release.apk_.

### Local environment

It is assumed that _yarn_ is installed. First, clone this repository and then run the command

```
yarn install
```

For the next step, an Android emulator must already have been set up in Android Studio. If that is the case, then this app can be run in the emulator with

```
yarn android
```

## Future considerations

If time allowed, then StyleSheets or another approach to styling would have been used, as it currently has no coherent structure.

## Screenshots

### Loading screen

<figure>
  <img src="readme_images/loading_screen.png" alt="Architecture of Rexgen" style="display:block;float:none;margin-left:auto;margin-right:auto;width:70%;border-style:ridge" />
</figure>

### Homescreen

<figure>
  <img src="readme_images/homescreen.png" alt="Architecture of Rexgen" style="display:block;float:none;margin-left:auto;margin-right:auto;width:75%;border-style:ridge" />
</figure>

### Refresh with ten new messages

<figure>
  <img src="readme_images/updating.png" alt="Architecture of Rexgen" style="display:block;float:none;margin-left:auto;margin-right:auto;width:75%;border-style:ridge" />
</figure>
